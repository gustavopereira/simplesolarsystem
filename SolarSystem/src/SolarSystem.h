#pragma once
#include <vector>
#include "ofImage.h"
#include "ofGraphics.h"
#include "math\ofVec2f.h"
#include "GameObject.h"
#include "VectorHelper.h"
#include "LineRenderer.h"
#include "CircleRenderer.h"
#include "PhysicsHelper.h"

//Distances between
#define EARTH_DISTANCE_FROM_SUN 260
#define MOON_DISTANCE_FROM_EARTH 100

//Physics
#define GRAVITATIONAL_CONSTANT 6

//Masses
#define SUN_MASS 120
#define EARTH_MASS 10
#define MOON_MASS 5
#define ASTEROID_MASS 1.2f

//Orbit Time
#define EARTH_ORBIT_TIME 5
#define MOON_ORBIT_TIME 1

class SolarSystem {
public:
	//Consts
	const ofColor backgroundColor = ofColor(30, 30, 30);
	const ofColor drawColor = ofColor(255, 255, 255);

	//Sprites
	GameObject sun;
	GameObject earth;
	GameObject moon;
	GameObject asteroid;

	//Degrees
	float earthDegrees;
	float moonDegrees;

	//Velocities
	ofVec2f asteroidVelocity = ofVec2f(1, -1).normalize() * 40;

	//Debug
	vector<ofVec2f> asteroidTrajectory;
	LineRenderer lineRenderer;

	//Methods
	void init(ofVec2f screenSize);
	void update(float deltaTime);
	void draw();
};
