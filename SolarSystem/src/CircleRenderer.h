#pragma once
#include "ofGraphics.h"

class CircleRenderer
{
public:
	static	void draw(ofVec2f center, float radius, ofColor drawColor, ofColor backgroundColor, uint16_t thickness = 2, uint16_t resolution = 64)
	{
		ofSetCircleResolution(resolution);
		ofSetColor(drawColor);
		ofDrawCircle(center, radius);
		ofSetColor(backgroundColor);
		ofDrawCircle(center, radius - thickness);
		ofSetColor(drawColor);
	}
};