# README #

### What is this repository for? ###
Vectors and newton's law of universal gravitation study

### How do I get set up? ###

1. Setup Visual Studio 2015 and OpenFrameworks plugins -> http://openframeworks.cc/setup/vs/
2. Open SolarSystem.sln in VS 2015
3. You will get an error from VS -> "One or more projects in the solution were not loaded correctly. Please see the output window for details"
4. Select openFrameworksLib (unavailable) in Solution Explorer
5. Properties->File path-> Click on button [...]
6. Select path-> ...\of_v0.9.8_vs_release\libs\openFrameworksCompiled\project\vs\openframeworksLib.vcxproj
7. Right click on openFrameworksLib (unavailable) in Solution Explorer -> Reload Project