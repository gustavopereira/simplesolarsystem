#include "SolarSystem.h"

void SolarSystem::init(ofVec2f screenSize)
{
	//Load Images
	sun.sprite.load("SunSprite.png");
	earth.sprite.load("EarthSprite.png");
	moon.sprite.load("MoonSprite.png");
	asteroid.sprite.load("AsteroidSprite.png");

	//Initial values
	sun.setPosition(ofVec2f(screenSize.x / 2, screenSize.y / 2));
	asteroid.setPosition(ofVec2f(screenSize.x / 8, screenSize.y / 2));
}

void SolarSystem::update(float deltaTime)
{
	//Adding points to trajectory
	asteroidTrajectory.push_back(asteroid.getCenterPosition());
	lineRenderer.setPoints(&asteroidTrajectory);

	//Calculating and setting new pos to Earth and Moon
	ofVec2f newEarthPos = (VectorHelper::getVector2fByDegrees(earthDegrees) * EARTH_DISTANCE_FROM_SUN) + sun.getCenterPosition();
	ofVec2f newMoonPos = (VectorHelper::getVector2fByDegrees(moonDegrees) * MOON_DISTANCE_FROM_EARTH) + newEarthPos;
	earth.setPosition(newEarthPos);
	moon.setPosition(newMoonPos);

	//Calculating relative rotation to Vector.right of Earth and Moon | Range = (0...360)
	earthDegrees = fmod((earthDegrees + 360.0f / EARTH_ORBIT_TIME * deltaTime), 360);
	moonDegrees = fmod((moonDegrees + 360.0f / MOON_ORBIT_TIME * deltaTime), 360);

	//Getting Asteroid to Other direction vectors
	ofVec2f toSunVector = sun.getCenterPosition() - asteroid.getCenterPosition();
	ofVec2f toEarthVector = earth.getCenterPosition() - asteroid.getCenterPosition();
	ofVec2f toMoonVector = moon.getCenterPosition() - asteroid.getCenterPosition();

	//Getting force using newton's universal law of gravitation
	float toSunForce = PhysicsHelper::getForce(SUN_MASS, ASTEROID_MASS, GRAVITATIONAL_CONSTANT, toSunVector.length());
	float toEarthForce = PhysicsHelper::getForce(EARTH_MASS, ASTEROID_MASS, GRAVITATIONAL_CONSTANT, toEarthVector.length());
	float toMoonForce = PhysicsHelper::getForce(MOON_MASS, ASTEROID_MASS, GRAVITATIONAL_CONSTANT, toMoonVector.length());

	//Settinng new Asteroid velocity and translating asteroid
	asteroidVelocity = (asteroidVelocity + (toSunVector.normalize() * toSunForce) + (toEarthVector.normalize() * toEarthForce) + (toMoonVector.normalize() * toMoonForce));
	asteroid.translate(asteroidVelocity * deltaTime);
}

void SolarSystem::draw()
{
	//Orbit Lines
	CircleRenderer::draw(sun.getCenterPosition(), EARTH_DISTANCE_FROM_SUN, drawColor, backgroundColor);
	CircleRenderer::draw(earth.getCenterPosition(), MOON_DISTANCE_FROM_EARTH, drawColor, backgroundColor);

	//Orbit Times
	string earthTime = std::to_string(fmod(ofGetElapsedTimef(), EARTH_ORBIT_TIME));
	string moonTime = std::to_string(fmod(ofGetElapsedTimef(), MOON_ORBIT_TIME));
	ofDrawBitmapString(earthTime.substr(0, 3), sun.getCenterPosition() + (ofVec2f(0, 1) * EARTH_DISTANCE_FROM_SUN - 10));
	ofDrawBitmapString(moonTime.substr(0, 3), earth.getCenterPosition() + (ofVec2f(0, 1) * MOON_DISTANCE_FROM_EARTH - 10));

	//Asteroid Trajectory
	lineRenderer.draw();

	//Sprites
	sun.draw();
	earth.draw();
	moon.draw();
	asteroid.draw();
}