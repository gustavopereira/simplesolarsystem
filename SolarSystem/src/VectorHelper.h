#pragma once
#include "math\ofVec2f.h"

class VectorHelper {
public:
	static ofVec2f getVector2fByDegrees(float deg) { return getVector2fByRad(deg * DEG_TO_RAD); };
	static ofVec2f getVector2fByRad(float rad) { return ofVec2f(cos(rad), sin(rad)); };
};