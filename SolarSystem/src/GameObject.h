#pragma once
#include "math/ofVec2f.h"
#include "ofImage.h"
class GameObject
{
public:
	ofImage sprite;
	ofVec2f position;

	GameObject(ofImage p_sprite, ofVec2f p_position);
	GameObject();
	~GameObject();

	void setPosition(ofVec2f p_position);
	void translate(ofVec2f p_direction);
	ofVec2f getCenterPosition();
	void draw();
};

